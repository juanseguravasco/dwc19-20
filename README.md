# Repositorio de productos
En este repositorio están las distintas entregas de la práctica de productos, cada entrega con su commit. Os recomiendo que vayáis comparando los diferentes commits para ver los cambios necesarios para implementar las nuevas funcionalidades de cada práctica realizada. Los commits que tenemos son:

## inicio y anteriores
Corresponde a la **'entrega del ejercicio 2 - componentes'**: la aplicación separada en componentes (pero sin axios)

## axios 
La aplicación que hace uso de axios para conectarse al servidor json-server. Corresponde a la **'entrega del ejercicio 3 - Vue-cli y axios'**

## SPA con vue-router, sin editar
La aplicación con vue-router, convertida en una SPA. COrresponde a la **'entrega de la Práctica 1 - Productos'** pero sin ternimnar ya que falta la parte de editar y mostrar productos.

## Final con editar y mostrar
Es la entrega final de la **'entrega de la Práctica 1 - Productos'** con todas las especificaciones implementadas

## Validación del form y acabar Vuex
Se valida el formulario de productos con _Vee Validate_ y se usa _Vuex_. Es la **'entrega de la Práctica 3 - Vuex'** con todas las especificaciones implementadas


