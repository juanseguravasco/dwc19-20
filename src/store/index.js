import Vue from 'vue'
import Vuex from 'vuex'
import APIService from '../utils/APIService';

Vue.use(Vuex)

const store = new Vuex.Store({
    state: {
      products: [],
      loaded: false
    },
    getters: {
      getProductById: (state) => (id) => state.products.find(product => product.id == id),
    },
    mutations: {
      add(state, product) {
        state.products.push(product);
      },
      replace(state, product) {
        let index = state.products.findIndex(item => item.id === product.id);
        if (index >= 0) state.products.splice(index, 1, product);
      },
      delete(state, id) {
        let index = state.products.findIndex(product => product.id === id);
        if (index >= 0) state.products.splice(index, 1);
      },
      changeUnits(state, {id, units}) {
        let index = state.products.findIndex(product => product.id === id);
        if (index >= 0 && state.products[index].units+units >= 0) state.products[index].units+=units;
      },
      load(state, products) {
        state.products = products;
      }
    },
    actions: {
      getProducts(context) {
        APIService.getProducts()
                .then(response=>{
                  context.commit('load', response.data);
                  context.state.loaded=true;
                })
                .catch(err=>alert('Error del servidor: '+err.message?err.message:err));
      },
      addProduct({commit}, product) {
        APIService.addProduct(product)
                .then(response=>{
                    commit('add', response.data);
                })
                .catch(err=>alert('Error del servidor: '+err.message?err.message:err));

      },
      updateProduct({commit}, product) {
        APIService.modifyProduct(product)
                .then(response=>{
                    commit('replace', response.data);
                })
                .catch(err=>alert('Error del servidor: '+err.message?err.message:err));

      },
      delProduct({commit}, id) {
        APIService.delProduct(id)
                .then(()=>{
                    commit('delete', id);
                })
                .catch(err=>alert('Error del servidor: '+err.message?err.message:err));

      }
    }
  })
export default store