import Vue from 'vue'

export default Vue.filter('currency', value => (isNaN(value)?'--':value.toFixed(2)) +' €');
