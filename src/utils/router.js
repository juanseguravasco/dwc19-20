import Vue from 'vue';
import Router from 'vue-router';
import AppHome from '../components/AppHome'
import AppAbout from '../components/AppAbout'
import ProductsTable from '../components/ProductsTable'
import ProductNew from '../components/ProductNew'
import ProductView from '../components/ProductView'
import NotFound from '../components/NotFound'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
	{
		path: '/',
		name: 'home',
		component: AppHome
	},{
		path: '/about',
		name: 'about',
		component: AppAbout
	},{
		path: '/products',
		component: ProductsTable
	},{
		path: '/new',
		component: ProductNew
	},{
		path: '/edit/:id',
		component: ProductNew,
		props: true,
    },{
        path: '/show/:id',
        component: ProductView,
        props: true,
    },{
		path: '/not-found',
		name: '404',
		component: NotFound
	},{
		path: '*',
		redirect: {
			name: '404'
		}
	}
  ],
})
