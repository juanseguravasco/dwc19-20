import axios from 'axios';
const API_URL = 'http://localhost:3000';

export default {
  getProducts() {
    return axios.get(API_URL+'/products')
  },

  getProduct(id) {
    return axios.get(API_URL+'/products/'+id)
  },

  delProduct(id){
    return axios.delete(API_URL+'/products/'+id)
  },

  addProduct(newProduct) {
    return axios.post(API_URL+'/products', newProduct)
  },

  modifyProduct(product) {
    return axios.put(API_URL+'/products/'+product.id, product);
  },
  
  changeProductUnits(product, increment) {
    const modifiedProduct=Object.assign({}, product);
    modifiedProduct.units+=increment;
    return axios.put(API_URL+'/products/'+product.id, modifiedProduct);
  },
  
}
